# Whisper CPP `large-v3*` hallucinations

A collection of hallucinations I've encountered with the `large-v3*` learning
model in [Whisper CPP](https://github.com/ggerganov/whisper.cpp).

(Also, this is an excuse for me to at least create 1 project on GitLab.)

## Purpose
General requirements will be that the lines have to be taken from a `large-v3*`
learning model, in which Whisper CPP repeated the same line over and over until
the very end of the transcription process.  Smaller "stutter" hallucinations
that end before Whisper CPP transcribes normally may be considered in the
future, if they are interesting in some way.  (However, "self-contained"
stutters tend to foreshadow a "terminal" stutter that lasts until the very end
of a recording.)  The lines also cannot contain any sensitive information.

This will be maintained until the unusually high rate of repeating
hallucinations are effectively stopped in `large-v3*`.

### Some references
* This [comment](https://github.com/openai/whisper/discussions/1762#discussioncomment-7532295) from OpenAI Whisper GH discussion [#1762](https://github.com/openai/whisper/discussions/1762)
* Whisper CPP GH discussion [#1911](https://github.com/ggerganov/whisper.cpp/discussions/1911)
* Whisper CPP GH issue [#1507](https://github.com/ggerganov/whisper.cpp/issues/1507)
* Whisper CPP GH issue [#1515](https://github.com/ggerganov/whisper.cpp/issues/1515)
* Whisper CPP GH issue [#1824](https://github.com/ggerganov/whisper.cpp/issues/1824)
* Whisper CPP GH issue [#1853](https://github.com/ggerganov/whisper.cpp/issues/1853)

#### Additional reading
This [comment](https://github.com/ggerganov/whisper.cpp/issues/1592#issuecomment-1839847717)
from Whisper CPP GH issue [#1592](https://github.com/ggerganov/whisper.cpp/issues/1592),
leading to [arXiv](https://en.wikipedia.org/wiki/ArXiv) paper
[arXiv:2311.14648v3](https://arxiv.org/abs/2311.14648v3).

## Quotes
* "If you were a board member of Tesla"

## License
Coming soon (probably a [Creative Commons](https://en.wikipedia.org/wiki/Creative_Commons) license)

## Project status
This will not be a very active project.  I will update this as I find interesting
and/or funny hallucinations while doing transcription work.   In the future,
anyone who has collaborated with me can submit a suggestion following the
guidelines above.

